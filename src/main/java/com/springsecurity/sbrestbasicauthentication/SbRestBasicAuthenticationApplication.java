package com.springsecurity.sbrestbasicauthentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbRestBasicAuthenticationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbRestBasicAuthenticationApplication.class, args);
	}

}
