package com.springsecurity.sbrestbasicauthentication.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
    @Autowired
    private AuthenticationEntryPoint authenticationEntryPoint;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        // Tất cả mọi request gửi tới Server đều yêu cầu xác thực
        http.authorizeRequests().anyRequest().authenticated();
        http.httpBasic().authenticationEntryPoint(authenticationEntryPoint);
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder authenticationManager) throws Exception {
        String password = "@bc123";
        String encodedPassword = this.passwordEncoder().encode(password);
        System.out.println("Encoded password of: " + password + " is: " + encodedPassword);

        InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> manageConfig = authenticationManager.inMemoryAuthentication();
        
        //Tạo người dùng vào lưu vào bộ nhớ
        //Spring auto add role
        UserDetails firstUser = User.withUsername("Dino").password(encodedPassword).roles("USER").build();
        UserDetails secondUser = User.withUsername("Dean").password(encodedPassword).roles("USER").build();

        manageConfig.withUser(firstUser);
        manageConfig.withUser(secondUser);
    }
}