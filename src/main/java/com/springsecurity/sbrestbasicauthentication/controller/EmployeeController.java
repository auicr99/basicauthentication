package com.springsecurity.sbrestbasicauthentication.controller;

import java.util.List;
import com.springsecurity.sbrestbasicauthentication.dao.EmployeeDAO;
import com.springsecurity.sbrestbasicauthentication.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;

@RestController
public class EmployeeController {
    @Autowired
    EmployeeDAO employeeDAO;

    @GetMapping("/")
    @ResponseBody
    public String welcome(){
        return "Welcome to EMS";
    }

    @RequestMapping(value = "/getEmployee/{empNo}", method = RequestMethod.GET, produces = {"application/json"})
    @ResponseBody
    public Employee getEmployee(@PathVariable("empNo") String empNo) {
        return employeeDAO.getEmployee(empNo);
    }
    
    @RequestMapping(value = "/getEmployeeList", method = RequestMethod.GET,produces = {"application/json"})
    @ResponseBody
    public List<Employee> getAllEmployee(){
        return employeeDAO.getAllEmployees();
    }

    @RequestMapping(value = "/addEmployee", method = RequestMethod.POST, produces = {"application/json"})
    @ResponseBody
    public Employee addEmployee(Employee employee){
        System.out.println("Server side - creating new employee: " + employee.getEmpNo());
        return employeeDAO.addEmployee(employee);
    }

    @RequestMapping(value = "/editEmployee/{empNo}", method = RequestMethod.PUT, produces = {"application/json"})
    @ResponseBody
    public Employee editEmployee(Employee employee){
        System.out.println("Server side - editing new employee: " + employee.getEmpNo());
        return employeeDAO.updateEmployee(employee);
    }

    @RequestMapping(value = "/deleteEmployee/{empNo}", produces = {"application/json"})
    @ResponseBody
    public void deleteEmployee(@PathVariable("empNo") String empNo){
        System.out.println("Server side - deleting new employee: " + empNo);
        employeeDAO.deleteEmployee(empNo);
    }
}