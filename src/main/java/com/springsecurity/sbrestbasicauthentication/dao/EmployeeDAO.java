package com.springsecurity.sbrestbasicauthentication.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.springsecurity.sbrestbasicauthentication.model.Employee;

import org.springframework.stereotype.Repository;

@Repository
public class EmployeeDAO {
    private static final Map<String, Employee> employeeMap = new HashMap<String, Employee>();

    static {
        initialEmployeeData();
    }

    private static void initialEmployeeData(){
        for(int i = 1; i < 5; i++){
            Employee employee = new Employee("E0" + i, "Dean V0" + i, i == 2 ? "Manager" : "Clerk");
            employeeMap.put(employee.getEmpNo(), employee);
        }
    }

    public Employee getEmployee(String empNo){
        return employeeMap.get(empNo);
    }

    public Employee addEmployee(Employee employee){
        employeeMap.put(employee.getEmpNo(), employee);
        return employee;
    }

    public Employee updateEmployee(Employee employee){
        employeeMap.put(employee.getEmpNo(), employee);
        return employee;
    }

    public void deleteEmployee(String empNo){
        employeeMap.remove(empNo);
    }

    public List<Employee> getAllEmployees(){
        Collection<Employee> empCollection = employeeMap.values();
        List<Employee> empList = new ArrayList<Employee>();
        empList.addAll(empCollection);
        return empList;
    }
}
